<?php
header('Content-Type: application/json');

require_once '../src/DBConnector.php';
require_once '../src/Data.php';

$connector = new DBConnector();
$latestData = $connector->getLatestData();

$hours = isset($_GET['hours']) && is_numeric($_GET['hours']) ? $_GET['hours']: 2;
$lastdays = $connector->getLastDays($hours);

$data = [
    'latest' => $latestData,
    'log' => []
];

//Prepare log for graph representation
foreach ($lastdays as $sensorData) {
    $data['log']['temperature'][] = ['x' => $sensorData->getTimestamp()->format(\DateTime::ISO8601), 'y' => $sensorData->getTemperature()];
    $data['log']['humidity'][] = ['x' => $sensorData->getTimestamp()->format(\DateTime::ISO8601), 'y' => $sensorData->getHumidity()];
}  

echo json_encode($data, JSON_NUMERIC_CHECK);