<?php
require_once '../src/DBConnector.php';
require_once '../src/Data.php';

// Only allow numbers as input
$hours = isset($_GET['hours']) && is_numeric($_GET['hours']) ? $_GET['hours']: 2;

$availableLastHours = [2, 8, 12, 24, 72, 24*7, 24*7*4];

$formatHours = function(int $hours) {
    return ($hours > 24 ? ($hours/24).' Tage' : $hours.'h');
}

?>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <script src="js/moment.min.js"></script>
    <script src="js/Chart.min.js"></script>
    <script src="js/hammer.min.js"></script>
    <script src="js/chartjs-plugin-zoom@0.7.7"></script>  

	<style>
	canvas {
		-moz-user-select: none;
		-webkit-user-select: none;
		-ms-user-select: none;
	}
	</style>
</head>
<body>
<div class="w3-container w3-center">
    <div class="w3-panel">

        <h1>Status vom: <span id="latestTimestamp"></span></h1>
        <div class="w3-light-grey w3-xlarge">
            <div id="latestStatusBar"></div>
        </div> 
    </div>
</div>
<div class="w3-container">
    <h2 class="w3-center">Verlauf letzte <?php echo $formatHours($hours); ?></h2>
    <canvas id="line_chart_div"></canvas>
</div>
<div class="w3-container w3-center">
<?php
    foreach ($availableLastHours as $availableHour) {
            if ($availableHour != $hours) {
                echo '<a href="/?hours='.$availableHour.'" class="w3-button w3-black">'.($formatHours($availableHour)).'</a> ';
            }
        }
?>
 </div> 

    <script type="text/javascript">       
    var config = {
        // The type of chart we want to create
        type: 'line',
        // The data for our dataset
        data: {
            datasets: [
                {
                    label: 'Temperatur [°C]',
                    borderColor: 'rgb(255, 0, 0)',
                    borderWidth: 2,
                    fill: false,
                    data: <?php echo json_encode($data['temperature'], JSON_NUMERIC_CHECK); ?>,
                    yAxisID: 'y-axis-temperature',
                },
                {
                    label: 'Luftfeuchtigkeit [%]',
                    borderColor: 'rgb(0, 0, 255)',
                    borderWidth: 2,
                    fill: false,
                    data: <?php echo json_encode($data['humidity'], JSON_NUMERIC_CHECK); ?>,
                    yAxisID: 'y-axis-humidity',
                }
            ]
        },
        options: {
            responsive: true,
            aspectRatio: 2,
            elements: {
                point: {
                    radius: 0
                }
            },
            scales: {
                xAxes: [{
                    type: 'time',
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Zeit'
                    },
                    time: {
                        unit: false,
                        tooltipFormat: 'dd DD.MM.yyy HH:mm:ss',
                        //define our own 24h date format
                        displayFormats: {
                            millisecond: 'HH:mm:ss.SSS',
                            second: 'HH:mm:ss',
                            minute: 'HH:mm',
                            hour: 'HH',
                            day: 'MMM D',
                            week: 'll',
                            month: 'MMM YYYY',
                            quarter: '[Q]Q - YYYY',
                            year: 'YYYY'
                        }
                    },
                }],
                yAxes: [{
                        labelString: '℃',
                        type: 'linear', // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
                        display: true,
                        position: 'right',
                        id: 'y-axis-temperature',
                        // grid line settings
                        gridLines: {
                            drawOnChartArea: true, 
                        },
                        // ticks: {
                        //     min: 10,
                        //     max: 70,

                        //     // forces step size to be 5 units
                        //     stepSize: 5
                        // }
                    }, 
                    {
                        labelString: '%',
                        type: 'linear', // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
                        display: true,
                        position: 'left',
                        id: 'y-axis-humidity',
                        // grid line settings
                        gridLines: {
                            drawOnChartArea: true, 
                        },
                        // ticks: {
                        //     min: 10,
                        //     max: 70,

                        //     // forces step size to be 5 units
                        //     stepSize: 5
                        // }
                    }
                ]
            },
            animation: {
                duration: 0 // general animation time
            },
            hover: {
                animationDuration: 0 // duration of animations when hovering an item
            },
            responsiveAnimationDuration: 0 // animation duration after a resize
            ,
            plugins: {
            zoom: {
                zoom: {
                    enabled: true,
                    mode: 'x'
                },
                pan: {
                    enabled: true,
                    mode: 'x'
                }
            }
        }
        }
    };

    function updateData() {
        fetch("<?php echo 'data.php?hours='.$hours ?>").then(r => r.json())
            .then(function(data){
                //Set bar data
                document.getElementById("latestTimestamp").innerHTML = moment(data.latest.timestamp).format('DD.MM.yyy HH:mm:ss');
                document.getElementById("latestStatusBar").innerHTML = data.latest.humidity + "% @ " + data.latest.temperature + "°C";
                document.getElementById("latestStatusBar").className = 'w3-container w3-'+(data.latest.humidity > 50 ? (data.latest.humidity > 60 ? 'red' : 'orange') : 'green');
                document.getElementById("latestStatusBar").style.width = data.latest.humidity + "%";

                //Update chart
                let chart = window.myLine;
                chart.data.datasets[0].data = data.log.temperature;
                chart.data.datasets[1].data = data.log.humidity;
                chart.update();
            })
            .catch(e => console.error("error loading data"));
    }

    window.onload = function() {
        var ctx = document.getElementById('line_chart_div').getContext('2d');
        window.myLine = new window.Chart(ctx, config);
        updateData();
        window.setInterval(function(){updateData()}, 20000);
    };
    </script>
</body>
</html>
