<?php

/**
 * Class Data
 */
class Data implements \JsonSerializable
{
    /**
     * @var float
     */
    private $temperature;

    /**
     * @var float
     */
    private $humidity;

    /**
     * @var DateTime
     */
    private $timestamp;

    /**
     * Data constructor.
     * @param float $temperature
     * @param float $humidity
     * @param DateTime $timestamp
     */
    public function __construct($temperature, $humidity, DateTime $timestamp)
    {
        $this->temperature = $temperature;
        $this->humidity = $humidity;
        $this->timestamp = $timestamp;
    }

    /**
     * @return float
     */
    public function getTemperature()
    {
        return $this->temperature;
    }

    /**
     * @return float
     */
    public function getHumidity()
    {
        return $this->humidity;
    }

    /**
     * @return DateTime
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    public function jsonSerialize () : array
    {
        return [
            'temperature' => $this->temperature,
            'humidity' => $this->humidity,
            'timestamp' => $this->timestamp->format(\DateTime::ISO8601)
        ];
    }
}
