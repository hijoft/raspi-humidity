<?php

/**
 * Class DBConnector
 */
class DBConnector
{
    private $servername = "localhost";
    private $username = "web-read";
    private $password = "PWp#4qTy7KUm9T@";
    private $dbname = "humidity_templogger";
    private $connector;

    public function __construct()
    {
        $this->connector = new mysqli($this->servername, $this->username, $this->password, $this->dbname);

        // Check connection
        if ($this->connector->connect_error) {
            throw new \Exception("DB Connection failed");
        }
    }

    public function getLastDays(int $hours): array
    {
        $now = new DateTime('now');
        $now->modify('-'.$hours.' hours');
        $sql = "SELECT temperature, humidity, timestamp FROM temp_humidity WHERE timestamp >= '".$now->format('Y-m-d H:i:s')."' ORDER BY timestamp ASC";
        $result = $this->connector->query($sql);

        //var_dump($this->connector->error);

        $data = [];
        // output data of each row
        while($row = mysqli_fetch_assoc($result)) {
            $data[] = new Data($row['temperature'], $row['humidity'], new DateTime($row['timestamp']));
        }

        return $data;
    }

    public function getLatestData(): ?Data
    {
        $sql = "SELECT temperature, humidity, timestamp FROM temp_humidity ORDER BY timestamp DESC LIMIT 1";
        $result = $this->connector->query($sql);

        //var_dump($this->connector->error);

        $data = null;
        // output data of each row
        while($row = mysqli_fetch_assoc($result)) {
            $data = new Data($row['temperature'], $row['humidity'], new DateTime($row['timestamp']));
        }

        return $data;
    }

    public function __destruct()
    {
        $this->connector->close();
    }
}
