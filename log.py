# Hijoft adapted script
# SPDX-FileCopyrightText: 2021 ladyada for Adafruit Industries
# SPDX-License-Identifier: MIT
    
import time
import sys
import board
import mysql.connector
import adafruit_dht

arguments = len(sys.argv)
if arguments > 1 and arguments != 5:
  print('Please provide 4 arguments for saving into database: <db-host> <db-user> <db-password> <db-name>')
  sys.exit(2)

# Try to connect if db parameters were provided
if arguments == 5:
  mydb = mysql.connector.connect(
    host=sys.argv[1],
    user=sys.argv[2],
    password=sys.argv[3],
    database=sys.argv[4]
  )
else:
  mydb = None

# Initial the dht device, with data pin connected to:
dhtDevice = adafruit_dht.DHT22(board.D4)
    
found = False
while found==False:
    try:
        # Print the values to the serial port
        temperature = dhtDevice.temperature
        humidity = dhtDevice.humidity
        print(
            "Temp: {:.1f} C    Humidity: {}% ".format(
                temperature, humidity
            )
        )

        if mydb is not None:
          # Write to database
          mycursor = mydb.cursor()
          sql = "INSERT INTO temp_humidity (temperature, humidity) VALUES (%s, %s)"
          val = (temperature, humidity)
          mycursor.execute(sql, val)
          mydb.commit()
          print("1 record inserted, ID:", mycursor.lastrowid) 

        found = True
        continue
    except RuntimeError as error:
        # Errors happen fairly often, DHT's are hard to read, just keep going
        print(error.args[0])
        time.sleep(2.0)
        continue
    except Exception as error:
        dhtDevice.exit()
        raise error
