SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";

CREATE TABLE `temp_humidity` (
  `id` int(11) NOT NULL COMMENT 'Identifier',
  `temperature` decimal(3,1) NOT NULL,
  `humidity` decimal(3,1) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE `temp_humidity`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `temp_humidity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identifier';
COMMIT;