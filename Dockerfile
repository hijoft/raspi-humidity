FROM php:7.4

COPY . /root

RUN chmod -R 775 /root/public && docker-php-ext-install mysqli

WORKDIR /root/public

CMD [ "php", "-S", "0.0.0.0:8080" ]